import { environment } from './../environments/environment.prod';
import { HttpClient, HttpRequest, HttpEvent } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { observable, Observable } from "rxjs";
import { Employee } from "./employee";

@Injectable({
    providedIn: 'root'
})
export class EmployeeService{
    private apiServerUrl=environment.apiBaseurl;

    constructor(private http: HttpClient ){}

   public upload(fileId:string,file: File): Observable<HttpEvent<any>> {
        const formData: FormData = new FormData();
        formData.append('id',fileId );
        formData.append('file', file);
        const req = new HttpRequest('POST',`${this.apiServerUrl}/employee/avatar`, formData, {
          reportProgress: true,
          responseType: 'text'
        });
        return this.http.request(req);
      }

    public addimage(uploadImageData:any):Observable<string>{
     return this.http.post(`${this.apiServerUrl}/employee/avatar`, uploadImageData,{responseType: 'text'} );
    }
 
    public getEmployees(): Observable<Employee[]>
    {
        return this.http.get<any>(`${this.apiServerUrl}/employee`);
    }

    public addEmployee(employee:Employee): Observable<Employee>
    {
        return this.http.post<Employee>(`${this.apiServerUrl}/employee/add`,employee);
    }

    public updateEmployee(employee:Employee): Observable<Employee>
    {
        return this.http.put<Employee>(`${this.apiServerUrl}/employee/update`,employee);
    }
    public deleteEmployee(employeeid: string): Observable<void>
    {
        return this.http.delete<void>(`${this.apiServerUrl}/employee/delete/${employeeid}`);
    }
}