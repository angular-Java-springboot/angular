import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmployeeService } from './employee.service';
import { HttpErrorResponse, HttpEventType, HttpResponse } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public employees!: Employee[];
  public editEmployee!: Employee;
  public deleteEmployee!: Employee;
  public selectedFile!: File ;
  public fileInfos!: Observable<any>;
  public hasFile:boolean=false;
  public progress = 0;
  public message = '';
  constructor(private employeeService: EmployeeService){}

  ngOnInit() {
    this.getEmployees();
  }

   //Gets called when the user selects an image
   public onFileChanged(event:any) {
    //Select File
    this.selectedFile = event.target.files[0];
    this.hasFile=true;
    this.upload();
  }

  upload(): void {
    this.progress = 0;
   
    this.employeeService.upload("623da86c43cffd11fb70aaf1",this.selectedFile).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          const total: number = event.total!;  
          this.progress = Math.round(100 * event.loaded /total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
        //  this.fileInfos = this.uploadService.getFiles();
        }
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.hasFile=false;
        //this.selectedFile = undefined;
      });
   
  }

  public getEmployees(): void {
    this.employeeService.getEmployees().subscribe(
      (response: Employee[]) => {
        this.employees = response;
        console.log(this.employees);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public addImage(id:string):void  {
     //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
     if (!this.hasFile) return;
     const uploadImageData = new FormData();
     uploadImageData.append('id',id );
     uploadImageData.append('file', this.selectedFile, this.selectedFile.name);
     console.log(uploadImageData);
     this.employeeService.addimage (uploadImageData).subscribe(
       (response: string) => {
         console.log(response);
         this.hasFile=false;
       },
       (error: HttpErrorResponse) => {
         alert(error.message);
       }
     );

  }

  public onAddEmloyee(addForm: NgForm): void {
    const myAbsolutelyNotNullElement = document.getElementById('add-employee-form');
    myAbsolutelyNotNullElement?.click();
    this.employeeService.addEmployee(addForm.value).subscribe(
      (response: Employee) => {
        console.log(response);
        this.addImage(response.id);
        addForm.reset();
        this.getEmployees();

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    );
  }

  public onUpdateEmloyee(employee: Employee): void {
    this.employeeService.updateEmployee(employee).subscribe(
      (response: Employee) => {
        console.log(response);
        this.addImage(employee.id);
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onDeleteEmloyee(employeeId: string): void {
    this.employeeService.deleteEmployee(employeeId).subscribe(
      (response: void) => {
        console.log(response);
        this.getEmployees();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public searchEmployees(key: string): void {
    console.log(key);
    const results: Employee[] = [];
    for (const employee of this.employees) {
      if (employee.name.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.email.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.phone.toLowerCase().indexOf(key.toLowerCase()) !== -1
      || employee.jobTitle.toLowerCase().indexOf(key.toLowerCase()) !== -1) {
        results.push(employee);
      }
    }
    this.employees = results;
    if (results.length === 0 || !key) {
      this.getEmployees();
    }
  }

  public onOpenModal(employee: Employee, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    if (mode === 'add') {
      button.setAttribute('data-target', '#addEmployeeModal');
    }
    if (mode === 'edit') {
      this.editEmployee = employee;
      button.setAttribute('data-target', '#updateEmployeeModal');
    }
    if (mode === 'delete') {
      this.deleteEmployee = employee;
      button.setAttribute('data-target', '#deleteEmployeeModal');
    }
    container?.appendChild(button);
    button.click();
  }
  public onOpenModalForNew(): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');
    button.setAttribute('data-target', '#addEmployeeModal');
    container?.appendChild(button);
    button.click();
  }
  



}